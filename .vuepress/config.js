module.exports = {
    title: "Bloomberg Environment EHS RegTracker",
    base: "/rtkw/",
    dest: ".vuepress/rtkw/",
    locales: {
        '/': {
            title: 'Bloomberg Environment EHS RegTracker',
            description: 'Product Tour for Bloomberg Environment EHS RegTracker'
        }
    },
    head: [
        ['link', { rel: 'icon', href: `/logo.png` }],
        ['link', { rel: 'manifest', href: '/manifest.json' }],
        ['link', { rel: 'canonical', href: 'https://bb-tour-ehs-regtracker.netlify.com' }],
        ['meta', { name: 'theme-color', content: '#0d9ddb' }],
        ['meta', { name: 'apple-mobile-web-app-capable', content: 'yes' }],
        ['meta', { name: 'apple-mobile-web-app-status-bar-style', content: 'black' }],
        ['link', { rel: 'apple-touch-icon', href: `/icons/apple-touch-icon-152x152.png` }],
        ['meta', { name: 'msapplication-TileImage', content: '/icons/msapplication-icon-144x144.png' }],
        ['meta', { name: 'msapplication-TileColor', content: '#000000' }],
        ['script', {src: 'https://assets.adobedtm.com/launch-EN164714b0147e4e5b8dce3e9da522ec62.min.js', async: 'async'}]
    ],
    serviceWorker: true,
    theme: 'bna',
    themeConfig: {
        docsDir: 'docs',
        activeHeaderLinks: false,
        sidebar: ['/docs/*',]
    }
}