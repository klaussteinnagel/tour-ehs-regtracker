---
title: RegTracker Administration
---
# RegTracker Administration

The **People** tab is the administrative center for RegTracker. From here
you can see everyone registered in the system, group membership, and a list of
people with administrator privileges.

All People shows contact information for everyone in the
system. People can edit contact information for themselves, but only
Administrators can edit information for others.

All Groups provides a way to organize, manage, and notify
all group members of assignments and posted comments. 

Tracking Profile links to page that shows the scope of
coverage - start date, jurisdictions included, and topics - for RegTracker
content. [Note: if you do not see this section, then your company has set up a
custom profile instead. Your administrator can give you more information on the
scope of coverage for your company.]

All Attached Files lets you search for any file uploaded
to a Tracking Summary. You can search by date, title, file type and more.

## Roles and Permissions

Everyone in the system is assigned one of three roles - Administrator, Dispatcher, or Reviewer - with
various degrees of access. For example, Administrators are the only people who
can edit others contact information as well as their own.

### Administrators

Administrators have full access to all RegTracker features, including the People &
Permissions tab. They can manage groups, change permissions, and set the scope
of coverage for the content that feeds the inbox.

_Note_: There must be at least one administrator at all times.

### Dispatchers

Dispatchers have full access to all RegTracker features - except for permissions. They can review the
Company Inbox, dispatch items and make or modify assignments. From the People
tab, Dispatchers can edit their own contact information and view other general
information about people, groups, and the Tracking Profile.

### Reviewers

Reviewers bypass the Company Inbox and go directly to the My Assignments queue to see, comment on, and
assess rulemaking documents forwarded to them for review. From the People tab,
Dispatchers can edit their own contact information and view other general
information about people, groups, and the Tracking Profile.

_Note_: From the People tab, Reviewers and Dispatchers
can make changes to their own profiles. Only an administrator will be able to
update another person's group(s) and information or to change the Company
Tracking Profile.

## Administrator’s View

Administrators can manage people and groups, change permissions, and set the scope of coverage for the content that feeds into the inbox.

![rt-17](/rtkw/images/rt-17.png "rt-17") 

## Edit People

To edit a person's profile information, an administrator clicks All People from the People tab. Click the Edit link next to a person's name to update a person's contact information, group membership, or permissions, etc.

![rt-18](/rtkw/images/rt-18.png "rt-18") 

## Add Users

Please contact your sales representative or Bloomberg Environment Customer Care at **800-372-1033** to add additional users.

## Create Groups

Create groups to quickly and easily make assignments. For example, you can group users
by area of expertise.

From **All Groups** on the People tab, click on the green button labeled **Create New Group**.

Choose a name for the group, and then click Create Group to save.  

![rt-19](/rtkw/images/rt-19.png "rt-19") 

## Add and Delete Group Members

It is easy to add or remove members from groups on the All Groups in the System page. Click the Group Name to see all members in the group. Click Edit to add or delete group members.

![rt-20](/rtkw/images/rt-20.png "rt-20") 

To add a user, place a blue check mark next to their name.  To remove a user, click on the blue check mark to remove it from the list. Click Save. 
Note: When a user is removed from a group, items assigned to that group automatically disappear from that individual’s My Assignments tab. 

![rt-21](/rtkw/images/rt-21.png "rt-21") 

## Delete Groups

Eliminate unneeded groups from the **People** tab
by clicking All Groups, and then clicking the Edit button to the right of the
group title. From the open group on the All Groups screen, select Delete Group
on the bottom left corner of the page and press Save.

The group will be removed from the assignment list.

* _Note_: Even if you delete a group, the individual group members will still be assigned to the rule making documents previously assigned to the group.  
* _Note_: Deleting a group will remove it from the assignment/email list; however, it will not delete group members from the System.

## All Attached Files

A simple way to locate a file attached to any document is to search for it under _All Attached Files_ on the **People** tab.

Browse a list of all files, or using the filters above, select specific criteria to pinpoint specific files. Using the drop down
menus, filter files by the document to which they are attached (_Summary Title, State, File Name_) or by other criteria (_File Type, Added By, Date_).

## Choose Administrators

Easily manage the administrative role from the Administrators page.  
From the People tab, select Administrators to open the Set Administrator Rights page.  
From the list of all users, place a check next to the individuals that should have administrative rights (or click a checked box to remove administrative rights) and Save Changes.  
New administrators will get permissions on their next login.

## Selecting Topics and Jurisdictions for the Company Inbox

Quickly and easily choose the jurisdictions and topics for the rules and regulations that feed into your Company Inbox.  
To change your Company Tracking Profile settings, click People on the top of the page, and then choose Tracking Profile.  

![rt-22](/rtkw/images/rt-22.png "rt-22") 

_Note_: The selected topics and jurisdictions will also appear in the filter drop down menus.

1. Under Scope of Coverage: US Jurisdictions, place a check next to jurisdiction(s) you
would like to appear in the Company Inbox and in the RegTracker sorting Filters.  
2. Under Scope of Coverage: EHS Media, select the topic(s) to feed into the Company
Inbox. Click a checked box again to remove the check.  
3. When you are ready, press Save.  

_Inbox Updates_
* New topics will appear in the Filters as soon as you press Save.  
* New topics will be delivered to your Company Inbox on the next update. (Inboxes typically update overnight).

_Note_: For your convenience, existing updates for topics that have been
removed will stay in your inbox until you remove them. Also, jurisdictions and
topics you have removed from active tracking will remain on your filter list
even when there are no new updates.
