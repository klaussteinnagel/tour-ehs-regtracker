---
title: Getting Started
---
# Getting Started

## Overview

The Bloomberg Environment EHS RegTracker helps EHS compliance teams stay on the
cutting edge of regulatory changes. Evaluate, prioritize, assign and
collaborate on new rule implementation as the latest Bloomberg Environment
regulatory monitoring summaries arrive in a convenient Company Inbox.

Each rule has a Rulemaking Details page which keeps all information about that
rule in one central place. It is comprised of the latest Bloomberg Environment
summaries, regulatory actions, staff assessments and reviews, comments, and
links to full-text regulations as they become available.

Have a bird's eye view of all rulemaking documents your team is tracking or needs to
review from an **All Tracking** grid. Each team member can quickly
find rules they are tracking under **My Assignments**, or retrieve
regulations from the **Not Tracking Archive**.

You can track regulations not included in the _EHS RegTracker_ by adding your own _Custom Documents_.
  

Optionally, add related links and upload related documents to enhance your
research. After they are saved, Custom Documents can be assigned and tracked
just like other records.

Get started with the Bloomberg Environment EHS RegTracker by logging on and taking a [tour](/). 

![rt-1](/rtkw/images/rt-1.png "rt-1")

## Logging On

Enter [http://regtracker.bna.com](http://regtracker.bna.com/) on your web browser. (Note: For optimal user experience, use Google Chrome or Mozilla Foxfire)

Enter your user name and password.

## Taking a Tour

To take an online tour of the Bloomberg Environment EHS RegTracker, visit [https://tours.bloombergenvironment.com/rtkw/](https://tours.bloombergenvironment.com/rtkw/). 

