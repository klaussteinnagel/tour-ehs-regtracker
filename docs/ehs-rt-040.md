---
title: Export to Excel
---
# Export to Excel

Each Tracking Grid can be downloaded to Excel.

Download grid information - including regulation summaries, added comments, and other
line items - to Excel for further analysis.

You can print, manipulate, make notes on, email, etc. the exported Tracking Grid as
desired.

From the Tracking Grid of your choice, create a useful filter and then click _Export
to Excel_ to download all information to your local computer.
Manipulate and save downloaded documents as desired within Excel.

![rt-16](/rtkw/images/rt-16.png "rt-16") 
