---
title: Company Inbox
---
# Company Inbox

The _Company Inbox_ acts as a tracking gateway. The most recent summaries from Bloomberg Environment are sent to the
Company Inbox for review.

The Inbox is organized to let administrators and dispatchers quickly scan, review, and dispatch entries, or dive deeper in to the content
with links to the full-text, company assessments, and internal comments.

_Note: Reviewers do not have access to the Company Inbox._

![rt-2](/rtkw/images/rt-2.png "rt-2")  

The Inbox is customized for those jurisdictions and topics your company is following. It also includes new rules as well as updates to previously published rulemaking documents.
To view more inbox items, click the Previous or Next arrows (located on the lower part of the inbox). Display more items **Per Page** by clicking the drop down box to the right. 

![rt-3](/rtkw/images/rt-3.png "rt-3")  

## Status Icons and Tracking Status

![rt-4](/rtkw/images/rt-4.png "rt-4")  

Next to each Inbox entry is a quick status for that item. 

*  New Item: Indicates the first time a rule appears in EHS RegTracker  
*  Marked for Review: Indicates an update to a rule needs review  
*  Tracking Update: Indicates an update to a rule that is being tracked  
*  Updated Archive Item: Indicates an update to an archived item

## Dispatch

Below each inbox entry is a set of **Dispatch** links.  Dispatch links allow you to quickly address items in the Company Inbox. Choose to _track, review, not track, or archive_ Inbox items. Click on a link to place the rulemaking document into the correct tracking category. 

![rt-5](/rtkw/images/rt-5.png "rt-5")  

For your convenience, you can set the tracking status for many items at once using **Quick Dispatch**. 

![rt-6](/rtkw/images/rt-6.png "rt-6")  

1. Click the checkbox next to the items you would like to dispatch. (To select all items on the page, click the box next to **Quick Dispatch** at the top of the
page.  Click again to _deselect_.)  
2. Click the arrow next to **Quick Dispatch**.  
3. Select your dispatch option from the drop down menu.  

## Inbox Filter

Focus and personalize your Company Inbox with specialized, targeted filters. Choose to filter Inbox items by Publish Date, Jurisdiction, Topic, and Regulatory Action. 

## Applying FIlters to the Company Inbox

![rt-7](/rtkw/images/rt-7.png "rt-7")  
Setting a filter is easy. Use the down arrows under each category to select the information that will display in the Company Inbox.

1. Click the down arrow next to each category (or click a date field to select a calendar date).  
2. Check off the subtopic(s) that most interest you. _Click a box again to deselect it._ To select all items listed in the entire
menu, select All (at the top of the list). Click again to deselect.  
3. Click **Apply** to save the filter settings.

Filtered
results immediately display in the Company Inbox.

Note:
The last filter setting you selected before you close the browser will be saved
for the next session.

1. To change a filter, select the new topics and then press _Apply_.  
2. Setting a filter never 'erases' items; it only hides irrelevant items from the display.  
3. Once a filter is set, you may hide it to save space. Click _Filter Options_ and
   then choose _Hide Filter_ to minimize the filter; choose _Show Filter_ to redisplay it.  
4. Click _Clear Filters_ to clear all existing filters at once. Click _Apply._  