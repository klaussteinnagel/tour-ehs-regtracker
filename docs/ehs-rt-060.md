---
title: Bloomberg Environment Training & Product Support
---
# Bloomberg Environment Training & Product Support

The Bloomberg Environment Customer Support team is available
Mon.-Fri., 8:30 a.m. to 7:00 p.m. Eastern Time (excluding most federal
holidays), to serve your business, technical, training, research, and product
support needs.  

### How to Contact Us:

BLOOMBERG ENVIRONMENT Phone: 833.697.9560 (U.S. and Canada) 

Email: [help@bloombergenvironment.com](mailto:help@bloombergenvironment.com)

Customer Support: [http://www.BloombergEnvironment.com/contact](http://www.BloombergEnvironment.com/contact)

Online Support Forms: [http://www.BloombergEnvironment.com/contact/onlineforms.htm](http://www.BloombergEnvironment.com/contact/onlineforms.htm)

Training website: [http://www.BloombergEnvironment.com/training](http://www.BloombergEnvironment.com/training)

