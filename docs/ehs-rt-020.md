---
title: Rulemaking Details
---
# Rulemaking Details

Open a Rulemaking Summary in a new window by clicking the
title.

The
Rulemaking Details page shows all information about that rule in one central
location. Use the Rulemaking Details page to share research and read summaries,
make assignments, or to make comments on and re-prioritize regulations within
your organization. For easy sharing, you can also upload documents to each
record from your local computer.

Each Rulemaking Details page has four sections, explained below:

1. The Rulemaking Summary and Full-Text access links  
2. The Company Assessment box  
3. The Attachments section, and  
4. The Comments section.  

![rt-8](/rtkw/images/rt-8.png "rt-8")  

## Rulemaking Summary

Each
Summary has four important parts:

*  The _Summary Title_  
*  Those _Citations Affected_ by this regulatory development  
*  Bloomberg Environment _Summary_ text (digest of the rulemaking action written by Bloomberg BNA editors).  
*  _Show Related Summaries_ gives easy access to related versions of the document.

## Company Assessment	

The **Company Assessment** panel is listed to the left of each _Bloomberg Environment Summary_.  
The **Company Assessment** is a prioritizing panel that allows you to assign team members, set priorities, change status, and specify due dates as a regulation is being tracked.  
Below the Company Assessment heading are assessment categories for managing tracking status and analysis. List the Applicability, Tracking Status, Impact, etc. of the regulation for your organization.  

![rt-9](/rtkw/images/rt-9.png "rt-9")  

Each category has a drop down arrow with a list of response options.  Use the drop down arrows to
select a response/evaluation for each category.  
To select a status:

1. Click the down arrow next to a category to view a list of available responses  
2. Click to highlight a response and select it  
3. Click **Save** in the bottom right corner when complete

To assign a summary to the team member(s) responsible for review:  

1. Click the drop down arrow under the Assigned To field.
2. Check the box(es) next to the team member(s) or group(s) you wish to assign. 
_Check 'All' to select all team members at once; click again to deselect all team members._  
3. Optionally, add a due date in the Due Date field. Click the Date Due field to open a
clickable calendar. Select the date by which the assignment should be
completed.  
4. Click **E-mail notice** to send an email assignment to the selected team members.   
5. Click **Save** at the bottom right of the screen.

Clicking **Save** on
the Company Assessment sends the summary to the selected (Needs Review, Not
Tracking, etc.) tab.   Your responses can
be saved as many times as needed. A time stamp below the Company Assessment
records the name of the team member who made the latest change. 

Automatic email reminders are sent to the assigned
individual three days before the item is due. Additionally, the individual will
receive a reminder on the day the item is due. The assignee is also copied on
email reminders.


If you need to track items not included in your Inbox, you can
create a **Custom Document**. Enter a title, topic, and other key
information. Optionally, upload related documents and links to enhance your
research. Once they are saved, _Custom Documents_ can be tracked
and assigned just like other regulatory documents.

To add a custom document:  

1. Select the **All Tracking** tab from your home screen.  
2. Click **+Add Custom Document**. A new screen will display titled _Custom Document_.  
3. Complete mandatory fields and preferred optional fields.  
4. Click **Save.**  

![rt-10](/rtkw/images/rt-10.png "rt-10")  

![rt-11](/rtkw/images/rt-11.png "rt-11")  

## All Tracking

Items selected for Tracking appear on the **All Tracking** grid. Easily get an overview of all regulations your company is tracking, make new assignments, or simply browse from **_All Tracking_**.

![rt-12](/rtkw/images/rt-12.png "rt-12")  

## My Assignments

The heart of **_EHS RegTracker_** is _My Assignments_. Review your assigned regulations, store your research and make
comments from any _Summary Title_.

Filter your assignments for viewing using the **Filter by** box on the left side of the
page.  Summaries can be viewed by publish date, jurisdiction, topic, regulatory action, applicability, impact, last
modified by, and date due. Click **Save**.

Selected filters are saved in the system so you will see only your filtered content each time you open the page. 

To clear the filters, click the blue **Clear Filter** link, then click **Save**.

![rt-13](/rtkw/images/rt-13.png "rt-13")  

## Needs Review	

Items designated as _Reviewing_ appear on the **Needs Review** tracking grid to be evaluated and assigned
for further investigation. When you need more time to make an assignment, hold regulations in the **_Needs Review_** tab.

## Not Tracking

Items designated as _Not Tracking_ will be sent to the **Not Tracking** tab. From the **Not Tracking** tab,
you will be able to decide whether to save an item for later, or to delete it permanently.

## Change Tracking Status

Tracking status can easily be changed on any tab.

1. Click the blue link of your selected summary. A _Rulemaking Details_ pop up will appear.  
2. Adjust the Tracking Status, In-house Status, Assignment, Applicability, Impact and/or Likelihood using the drop down menus.  
3. Click **Save**.

![rt-14](/rtkw/images/rt-14.png "rt-14")  
