---
title: Custom Documents
---
# Custom Documents

For your convenience, you can track a regulation or requirement not included in the
EHS RegTracker by creating your own Custom Document.  
Custom Documents can be created on the **All Tracking** tab.  
The Custom Documents form lets you type in your own summary, add outside links, and
upload related documents. _You may upload a file of any type (except for .exe), as long as it is under 5 MB._  
Once the Custom Document is created, it can be assigned and tracked just like other items.

## Create Custom Documents

To add your own Custom Document, open the **All Tracking** tab, then click the **+Add Custom Document** icon. A new window will appear.

![rt-15](/rtkw/images/rt-15.png "rt-15") 

1. In the new window, type your document's **Title**.  
2. Scroll to select a **Jurisdiction, Topic** and **Regulatory Action**. These fields are required and are marked with an asterisk.

_If you cannot find an item in the drop down menu, click to select (Other)._  
_Note: The menu displays the Jurisdictions and Topics you are currently tracking on the People and
Permissions tab. To add new Jurisdictions or Topics to your Tracking Profile,
see the RegTracker Administration section of this guide._
  
3. Optionally, type in or paste in related Citations.   
4. Type in or paste in a brief Summary.   
5. **Add More Information** if desired.   
6. When you are ready, scroll to the bottom of the page and click **Save**. **Note: you must click Save.**  
7. A message will display: _"Your tracking summary has been added to the system. Add another summary?"_   
8. Click _Add another summary_ to add more custom documents, or, if you are finished, click _Close Window_.   
9. Click _Refresh Table_ ![rt-icon](/rtkw/images/rt-icon.png "rt-icon") to display the
new record on the _All Tracking_ tab.

For easy recognition, **Custom Tracking Documents** are marked with a green star in front of the Summary Title on each tracking grid.

## Edit Custom Documents

Editing ontent in your _Custom Documents_ is simple.

From the tracking grid, click a record's title and then choose **Edit** in
the new _Rulemaking Details _screen. 

Type new text in the _Title, Citations Affected,_ or _Summary_ fields
or scroll to select a new _Jurisdiction, Topic,_ or _Regulatory
Action_.

When you are ready, click **Save** to update the record.